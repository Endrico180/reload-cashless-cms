# frozen_string_literal: true

class Users::SessionsController < Devise::SessionsController
  # before_action :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
  def create
    user_email = User.where("email = ?", params[:user][:email]).first
    if user_email
      if user_email
        user = warden.authenticate!(auth_options)
        if user
          if session["url"]
            redirect_to session["url"]
          else
            redirect_to root_path
          end
        else
          flash[:error] = "Email or Password invalid"
          render 'new'
        end
      else
        warden.logout
        flash[:notice] = "You didn't have more access"
        redirect_to new_user_session_path
      end
    else
      warden.logout
      render json: {status: 402, messages: "Sorry, You didn't have more access"}
    end
  end

  # POST /resource/sign_in
  # def create
  #   super
  # end

  # DELETE /resource/sign_out
  def destroy
    sign_out(@user)
    @current_user = nil
    redirect_to new_user_session_path
  end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  # end
end
