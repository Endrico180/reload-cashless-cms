// Hide submenus
//$('#menu-wrapper .collapse').collapse('hide');

// Collapse/Expand icon
$('#collapse-icon').addClass('fa-angle-double-left');

// Collapse click
$('[data-toggle=sidebar-colapse]').click(function () {
	SidebarCollapse();
});

function SidebarCollapse() {
	$('.menu-collapsed').toggleClass('is-hidden');
	$('.sidebar-submenu').toggleClass('is-hidden');
	$('.submenu-icon').toggleClass('is-hidden');
	$('#sidebar-container').toggleClass('sidebar-expanded sidebar-collapsed');
	$('#main-content').toggleClass('main-content-expanded main-content-collapsed')
	// Treating d-flex/d-none on separators with title
	var SeparatorTitle = $('.sidebar-separator-title');
	if (SeparatorTitle.hasClass('is-flex')) {
		SeparatorTitle.removeClass('is-flex');
	} else {
		SeparatorTitle.addClass('is-flex');
	}

	// Collapse/Expand icon
	$('#collapse-icon').toggleClass('fas fa-angle-double-left fas fa-angle-double-right');
}